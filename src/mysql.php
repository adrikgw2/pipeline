<?php
include "log.php";
$bdd_params_object = new stdClass;
$bdd_params_object->db_host = getenv('RDS_HOSTNAME');
$bdd_params_object->db_name = getenv('RDS_DB_NAME');
$bdd_params_object->db_port = getenv('RDS_PORT') ?: 3306;
$bdd_params_object->user = getenv('RDS_USERNAME');
$bdd_params_object->password = getenv('RDS_PASSWORD');
$bdd_params_object->connexion = 'host=';
$bdd_params_object->charset = 'utf8';

try {
    $bdd = new PDO('mysql:' . $bdd_params_object->connexion . $bdd_params_object->db_host . ';dbname=' . $bdd_params_object->db_name . ';charset=' . $bdd_params_object->charset, $bdd_params_object->user, $bdd_params_object->password);
} catch (Exception $e) {
    echo "ERREUR CONNEXION BDD";
    echo '<pre>';
    print_r($bdd_params_object);
    echo '</pre>';
    die('Erreur : ' . $e->getMessage());
}

$req = $bdd->prepare('SELECT name FROM city');
$req->execute();
var_dump($req->fetchAll());

$log->debug("MySQL");